<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css" type="text/css">
    <title>Document</title>
</head>
    <body class="body">
        <header class="header">
            <a href="#">
                <img class="img" src="https://dpo.mospolytech.ru/content/themes/default/img/logo_black.png" alt="logo">
            </a>
            <p class="text-header">
                <?php echo 'Работа №1'; ?>
            </p>
        </header>
        <main class="text-main">
            <?php echo 'Привет мир!'; ?>
        </main>
        <footer>
            <p class="text-footer">
                <?php echo 'Задание: Создать веб-страницу с динамическим контентом. Загрузить код в удаленный репозиторий. Залить на хостинг.'; ?>
            </p>  
        </footer>
    </body>
</html>